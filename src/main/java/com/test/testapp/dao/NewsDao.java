package com.test.testapp.dao;

import com.test.testapp.bean.News;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * @Project:testapp
 * @PackageName:com.test.testapp.dao
 * @Author:null z
 * @DateTime:2019/2/13 15:32.
 */
public interface NewsDao extends BaseMapper<News> {
}
