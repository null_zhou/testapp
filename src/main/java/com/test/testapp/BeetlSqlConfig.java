package com.test.testapp;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.alibaba.druid.support.spring.stat.DruidStatInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.ClasspathLoader;
import org.beetl.sql.core.Interceptor;
import org.beetl.sql.core.UnderlinedNameConversion;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.ext.spring4.BeetlSqlDataSource;
import org.beetl.sql.ext.spring4.BeetlSqlScannerConfigurer;
import org.beetl.sql.ext.spring4.SqlManagerFactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

/**
 * @Project:mart-company
 * @PackageName:mart.blue.company.start.config
 * @Author:null z
 * @DateTime:2019/2/13 11:01.
 */
@Slf4j
@Configuration
@Component
public class BeetlSqlConfig {


    @Bean
    public ServletRegistrationBean druidServlet() {
        ServletRegistrationBean reg = new ServletRegistrationBean();
        reg.setServlet(new StatViewServlet());
        reg.addUrlMappings("/druid/*");
        reg.addInitParameter("loginUsername", "asd");
        reg.addInitParameter("loginPassword", "asdasd");
        reg.addInitParameter("resetEnable", "true");
        return reg;
    }


    @Bean
    public DruidStatInterceptor druidStatInterceptor() {
        return new DruidStatInterceptor();
    }


    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new WebStatFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        filterRegistrationBean.addInitParameter("profileEnable", "true");
        filterRegistrationBean.addInitParameter("principalCookieName", "USER_COOKIE");
        filterRegistrationBean.addInitParameter("principalSessionName", "USER_SESSION");
        filterRegistrationBean.addInitParameter("sessionStatEnable", "true");
        return filterRegistrationBean;
    }


    @Bean(initMethod = "init", destroyMethod = "close")
    public DruidDataSource druidDataSource(@Value("${show.sql}") Boolean showSql) {
        System.out.println("===========>"+showSql);
        DruidDataSource druidDataSource = getDruidDataSource("com.mysql.jdbc.Driver", "jdbc:mysql://127.0.0.1:21406/test?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&zeroDateTimeBehavior=convertToNull&useSSL=false",
                "test", "test",
                "test", 5000, 5,
                5, 500, 500
                , 5000,
                30001, 60);
        return druidDataSource;
    }


    /**
     * 统一配置项
     *
     * @param driver
     * @param url
     * @param username
     * @param password
     * @param name
     * @param maxActive
     * @param minIdle
     * @param initialSize
     * @param maxWait
     * @param maxOpenPreparedStatements
     * @param timeBetweenEvictionRunsMillis
     * @param minEvictableIdleTimeMillis
     * @param queryTimeout
     * @return
     */
    private DruidDataSource getDruidDataSource(String driver, String url, String username, String password, String name,
                                               Integer maxActive, Integer minIdle, Integer initialSize, Integer maxWait, Integer maxOpenPreparedStatements,
                                               Integer timeBetweenEvictionRunsMillis, Integer minEvictableIdleTimeMillis, Integer queryTimeout) {
        DruidDataSource druidDataSource = new DruidDataSource();
        try {
            druidDataSource.setFailFast(true);
            druidDataSource.setKeepAlive(true);
            druidDataSource.setName(name);
            druidDataSource.setDriverClassName(driver);
            druidDataSource.setUrl(url);
            druidDataSource.setUsername(username);
            druidDataSource.setPassword(password);
            druidDataSource.setMaxActive(maxActive);
            druidDataSource.setMinIdle(minIdle);
            druidDataSource.setInitialSize(initialSize);
            druidDataSource.setMaxWait(maxWait);
            druidDataSource.setUseUnfairLock(true);
            druidDataSource.setPoolPreparedStatements(true);
            druidDataSource.setMaxPoolPreparedStatementPerConnectionSize(maxOpenPreparedStatements);
            druidDataSource.setMaxOpenPreparedStatements(maxOpenPreparedStatements);
            druidDataSource.setValidationQuery("SELECT 1");
            druidDataSource.setValidationQueryTimeout(30);

            druidDataSource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
            druidDataSource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
            //连接获取检查
            druidDataSource.setTestWhileIdle(true);

            druidDataSource.setTestOnBorrow(true);
            //连接归还检查
            druidDataSource.setTestOnReturn(true);
            //  <!-- 自动清除无用连接 -->
//            druidDataSource.setRemoveAbandoned(true);
            //  <!-- 清除无用连接的等待时间 -->
//            druidDataSource.setRemoveAbandonedTimeout(20000);
            //<!-- 每隔24小时把监控数据从内存中输出到日志中, (毫秒数)-->
            druidDataSource.setTimeBetweenLogStatsMillis(86400000);
            //查询超时 秒
            druidDataSource.setQueryTimeout(queryTimeout);
            //  <!-- 配置监控统计拦截的filters,wall-sql攻击 -->
            druidDataSource.setFilters("mergeStat,wall");
            // 输出查询
            druidDataSource.setConnectionProperties("druid.stat.slowSqlMillis=1000");
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            log.error("druid配置异常", e);
        }
        return druidDataSource;
    }


    @Bean("beetlsqlScanner")
    public BeetlSqlScannerConfigurer beetlsqlScanner() {
        BeetlSqlScannerConfigurer conf = new BeetlSqlScannerConfigurer();
        conf.setBasePackage("com.test.testapp");
        conf.setDaoSuffix("Dao");
        conf.setSqlManagerFactoryBeanName("sqlManagerFactoryBeanOne");
        return conf;
    }

    /**
     * factory.setNc方法
     * <p>
     * DefaultNameConversion 数据库名和java属性名保持一致，如数据库表User，对应Java类也是User，数据库列是sysytemId,则java属性也是systemId，反之亦然
     * UnderlinedNameConversion 将数据库下划线去掉，首字母大写，如数据库是SYS_USER（oralce数据库的表和属性总是大写的), 则会改成SysUser,字段名也是以驼峰命令方式映射
     * JPA2NameConversion 支持JPA方式的映射，适合不能用确定的映射关系(2.7.4以前采用JPANameConversion过于简单，已经不用了)
     * <p>
     * 在使用org.beetl.sql.core.JPA2NameConversion作为命令转化规则时，你可以使用以下JPA标签来帮助解析实体类到数据库的转换:
     * <p>
     * javax.persistence.Table
     * javax.persistence.Column
     * javax.persistence.Transient
     */
    @Bean(name = "sqlManagerFactoryBeanOne")
    public SqlManagerFactoryBean getSqlManagerFactoryBeanOne(@Qualifier("druidDataSource") DruidDataSource datasource, @Value("${show.sql}") Boolean showSql) {
        SqlManagerFactoryBean factory = new SqlManagerFactoryBean();
        BeetlSqlDataSource source = new BeetlSqlDataSource();
        source.setMasterSource(datasource);
        Resource resource = new ClassPathResource("bin/btsql.properties");
        factory.setConfigLocation(resource);
        factory.setCs(source);
        System.out.println(showSql);
        factory.setDbStyle(new MySqlStyle());
        if (showSql) {
            factory.setInterceptors(new Interceptor[]{});
        }
        factory.setNc(new UnderlinedNameConversion());
        factory.setSqlLoader(new ClasspathLoader("/companySql"));
        return factory;
    }


}
